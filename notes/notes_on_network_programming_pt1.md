1. Dr. Chuck mentions the architecture that runs our network. What is the name of this architecture?

The name of the architecture is the TCP (Transport Control Protocol)

2. Take a close look at the slide labelled Stack Connections. Which layer does Dr. Chuck say we will be looking at, assuming that one end of this layer can make a phone call to the corresponding layer on the other end? What are the names of the two lower level layers that we will be abstracting away?

Dr Chuck says we will be looking at the layer titled "Transportation" and the two lower level layers that we will be abstracting away are the Internet and Link layers

3. We will be assuming that there is a connection that goes from point A to point B. There is a process/program running at each end of the connection.


4. Define Internet socket as discussed in the video.

A bi-directional protocol of data, in which a process running on one computer speaks into thje socket and the other computer reads that and returns something back into the socket.

5. Define TCP port as discussed in the video.

A TCP port is a port that is an application specific or process specific software communications endpoint.

6. At which network layer do sockets exist?

Sockets exits at the Transportation network layer.

7. Which network protocol is used by the World Wide Web? At which network layer does it operate?

The World Wide Web uses the network protocol called TCP port 80 or 443, it operates on the transportation network layer.


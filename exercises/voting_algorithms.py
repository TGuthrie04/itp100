def plurality(election):
    """
    Return the plurality winner among three candidates, 1, 2, and 3. The input
    election is a list of three tuples, so in a plurality election only the
    first element matters.

      >>> plurality([(1, 2, 3), (2, 3, 1), (2, 1, 3)])
      2
      >>> plurality([(3, 2, 1), (3, 2, 1), (2, 1, 3)])
      3
    """
    # Count the votes for each of the three candidates in a dictionary
    vote_totals = {1: 0, 2: 0, 3: 0} 
    for vote in election:
        vote_totals[vote[0]] += 1

    # Return the candidate number with the highest vote count
    return max(vote_totals.items(), key=lambda x: x[1])[0]

def exhaustive_ballot(election):
    """
    Candidate with the least amount of votes gets eliminated
    Then goes onto another round where same thing happens
    Since there are only 3 candidates there will be two rounds of voting 

    >>> exhaustive_ballot([(1, 2, 3), (2, 1, 3), (2, 1, 3), (1, 2, 3), (3, 1, 2)])
    1
    >>> exhaustive_ballot([(1, 2, 3), (1, 3, 2), (3, 1, 2)])
    1
    """
    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election:
        vote_totals[vote[0]] += 1

    m = (min(vote_totals.items(), key=lambda x: x[1])[0])

    for key in vote_totals.keys():
        if key == m:
            del vote_totals[key]
            break

        
    for vote in election:
        if vote[0] == m:
            vote_totals[vote[1]] += 1

    
    return max(vote_totals.items(), key =lambda x: x[1])[0]

def onetwoprimary(election):
    """
    Candidates one and two face off in a primary then the winner face off against candidate 3
    If a voter first priorty was 3 then their second prioity is who they vote for in the primary
    If a voter first priorty is an elimanted candidate then move onto next priority

    >>> onetwoprimary([(1, 2, 3), (1, 3, 2), (2, 1, 3), (3, 1, 2)])
    1
    >>> onetwoprimary([(3, 2, 1), (3, 2, 1), (1, 2, 3)])
    3
    """
    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election:
        if vote[0] == 3:
            vote_totals[vote[1]] += 1
        else:
            vote_totals[vote[0]] += 1

    m = abs((max(vote_totals.items(), key=lambda x: x[1])[0])- (min(vote_totals.items(), key=lambda x: x[1])[0]))

    for key in vote_totals.keys():
        if key == m:
            del vote_totals[key]
            break

    vote_totals = {1: 0, 2: 0, 3: 0}

    for vote in election:
        if vote[0] == m:
            vote_totals[vote[1]] += 1
        else:
            vote_totals[vote[0]] += 1

    return max(vote_totals.items(), key=lambda x: x[1])[0]

def onethreeprimary(election):
    """
    Same as above except its candiates 1 and three in the primary

    >>> onethreeprimary([(1, 2, 3), (2, 1, 3), (3, 1, 2)])
    1
    >>> onethreeprimary([(3, 1, 2), (3, 2, 1), (2, 1, 3), (2, 1, 3), (1, 2, 3)])
    1
    """

    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election:
        if vote[0] == 2:
            vote_totals[vote[1]] += 1
        else:
            vote_totals[vote[0]] += 1

    m = (min(vote_totals.items(), key=lambda x: x[1])[1])

    for key in vote_totals.keys():
        if key == m:
            del vote_totals[key]
            break

    for vote in election:
        if vote[0] == m:
            vote_totals[vote[1]] += 1
        else:
            vote_totals[vote[0]] += 1

    return max(vote_totals.items(), key=lambda x: x[1])[0]

def twothreeprimary(election):
    """
    Same as above except its candidate 2 and 3 in the primary
    >>> twothreeprimary([(2, 1, 3), (2, 1, 3), (3, 1, 2)])
    2
    >>> twothreeprimary([(1, 3, 2), (1, 2, 3), (3, 1, 2), (2, 1, 3), (3, 2, 1)])
    3
    """

    vote_totals = {1: 0, 2: 0, 3: 0}
    for vote in election:
        if vote[0] == 1:
            vote_totals[vote[1]] += 1
        else:
            vote_totals[vote[0]] += 1

    m = (min(vote_totals.items(), key=lambda x: x[1])[1])

    for key in vote_totals.keys():
        if key == m:
            del vote_totals[key]
            break

    for vote in election:
        if vote[0] == m:
            vote_totals[vote[1]] += 1
        else:
            vote_totals[vote[0]] += 1

    return max(vote_totals.items(), key=lambda x: x[1])[0]



if __name__ == "__main__":
    import doctest
    doctest.testmod()



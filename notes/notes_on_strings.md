1. What does Dr. Chuck say about indexing strings? How does this operation work? Provide a few examples.
Indexing strings is a way to find a character in a string. It works by receving a number that corresponds to where a letter is in a string. For example 
fruit = 'orange'
letter = fruit[2]
print(letter)

it would return 
'a'

2. Which Python function returns the number or characters in a string? Provide a few examples of it being used.

The built in function len gives us the length of a string. Example

fruit = 'orange'
print(len(fruit))
'6'

3. Discuss the two ways Dr. Chuck shows to loop over the characters in a string. Provide an example of each.
Dr Chuck uses 2 different ways to loop over the characters in a string, a while loop and a for loop. Example

fruit = 'orange'
for letter in fruit
	print(letter


and

index = 0
while index < len(fruit)
	letter = fruit(index)
	print(letter)
	index = index + 1


4. What is slicing? Explain it in your own words and provide several examples of slicing at work. including examples using default values for start and end.

Slicing is the act of taking groups of characters in a string out rather than just single characters. Example
s = "Test String"
print(s[:]

5. What is concatenation as it applies to strings. Show an example.
Strings don't automatically add spaces like a print fucntion does with commas, to do this we must add spaces to the strings. This is called concatenation. Example

b = a + ' ' + Hello


6. Dr. Chuck tells us that in can be used as a logical operator. Explain what this means and provide a few examples of its use this way with strings.

The function "in" tells us if a chunk of characters is in a string by returning true or false.Example

word = 'Hello'
'll' in word
True

7. What happens when you use comparison operators (>, <, >=, <=, ==, !=) with strings?

Comparison operators comapre the number of characters in a string

8. What is a method? Which string methods does Dr. Chuck show us? Provide several examples.

A method is a something that you can do to a string that finds something in the string or affects it in someway. For example 

str.replace(old, new[,count])

9. Define white space.

Spaces or non-prtinting character like tabs, can be removed with a strip command (.lstrip and .rstrip)

10. What is unicode?

Unicode is the result of non-latin characters in a string, however this only occurs in python 2. In python 3, all strings are unicode already.


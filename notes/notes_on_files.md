1. What function does Python use for accessing files? What arguments does it need? What does it return?

Python uses the open() function to access and open files. It requires a handle, file name, and mode. It returns a handle used to manipulate the file.

2. What is a file handle? Explain how it relates to a file? Where is the data stored?

A file handle allows a python program to access data stored in a different file. The data is not stored in the handle and handle will not return the data in the file, it is simply a pathway for the python program to access a file. Similar to a variable 

3. What is '\n'?

The \n indicates that a new line is defined.

4. What does Dr. Chuck say is the most common way to treat a file when reading it?

Dr. Chuch says the most common way to treat a file is line by line. 

5. In the Searching Through a File (fixed) example, Dr. Chuck talks about the problem of the extra newline character that appears when we print out each line. He resolves this problem by using line.rstrip(), invoking Python's built-in rstrip method of strings. Could we also use a slice here, and write line[:-1] instead? Explain your answer.

Yes this could be done as this method of slicing removes the last character and \n is counted as a one character, meaning it would be removed. 

6. The second video presents three different ways, or patterns for selecting lines that start with 'From:'. Compare these three patterns, providing examples of each.

The first way Dr Chuck does this is by using the command
 
line.startswith('From')

This is a built in function that finds every line that starts with from. The next pattern is
 
if not line.startswith('From:') 
	continue
print(line)

This method asks if any lines do not have From rather than finding ones that do. The last pattern he uses is

if not '@uct.ac.za' in line :
	continue
print(line)

This method is similar to the last one, it checks for characters in a line and if they are not in that line the line is skipped and not printed.

7. A new Python construct is introduced at the end of the second video to deal with the situation when the program attempts to open a file that isn't there. Describe this new construct and the two new keywords that pair to make it.

The two new keywords are try and except and the pair to make a sort of insurance in case the file can not be opened because of a bad file name or other problem. 


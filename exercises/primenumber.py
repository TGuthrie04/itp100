import math
pnum = []

for num in range(2,1000):
    prime = True
    for i in range(2,num):
        if (num%i == 0):
            prime = False
    if prime:
        pnum.append(num)

pnum.insert(0, 0)

x = input("Enter N?")
x = int(x)
if x == 0:
    print("must be a value larger than 0")
else:
    print(pnum[x])


## The Problem : Formally, given a positive integer N, return the Nth prime. For example, N=I should return the
## number 2, N=2 should return the number 3, N=3 should return the number 5, and so on. You may
## assume that N < 216




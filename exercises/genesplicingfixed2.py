f = open("gene_splicing_test.dat")
line = f.readline()

for line in f:
    values = line.split()
    trials = 0
    a = float(values[0])
    b = float(values[1])
    c = float(values[2])
    t = float(values[3])

    target = a/b
    Amix = 1
    Bmix = 0
    
    while True:
        trials += 1
        
        a = Bmix * b + Amix * c
        newvol = b + c
        Bmix = (Bmix * b + Amix * c)/(b + c)

        a = Amix * a + Bmix * c
        newvol = a + c
        Amix = (Amix * a + Bmix * c)/(a + c)

        if Amix == 0.5 and Bmix == 0.5:
            break
        
    print(trials)

         

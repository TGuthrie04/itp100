import string

l = string.ascii_lowercase
u = string.ascii_uppercase

u = list(u)
l = list(l)
s = list(' ')


name = input('Enter file:')
handle = open(name)
flist = [line.strip() for line in handle]
key = int(flist[0])
lines = 1
check = 0


while True:

    if 'STOP\n' in flist[lines]:
        break
    else:
        final = []
        cyphertxt = []

        for char in flist[lines]:
            if char in u:
                index = u.index(char)
                cypher = (index + key) % 26
                cyphertxt.append(cypher)
                newLetter = u[cypher]
                final.append(newLetter)
            elif char in l:
                index = l.index(char)
                cypher = (index + key) % 26
                cyphertxt.append(cypher)
                newLetter = l[cypher]
                final.append(newLetter)
            elif char in s:
                final.append(' ')
            else:
                final.append(char)
        
        b = ''.join(final)
        print(b)
        lines += 1



        







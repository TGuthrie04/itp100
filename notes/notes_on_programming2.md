1. What is an application protocol? List examples of specific application protocols listed in the lecture. Can you think of one besides HTTP that we have been using in our class regularly since the beginning of the year?

An application protocol is a set of rules that all parties follow so we can predict each other's behavior and not mess eahc other up. Some examples given in the lecture are HTTP (Hyper Text Transfer Protocol), Mail, WWW, and File Transfer. I am unsure of what application protocol we have been using since the beggining of the year, possibly SSH? And if it isnt counted as the same then HTTPS.

2. Name the three parts of a URL. What does each part specify?

The three parts of a URL are protocol, host, and document. The protocol refers to which protocol is used to determine what application protocol this runs off of. The host is where the information is coming from and the document is what information from the host you are retriving.

3. What is the request/response cycle? What example does Dr. Chuck use to illustrate it and describe how it works?

When your computer makes a request to a web server through a specfic port, the server will then do the action specificed (read files, run code, etc.) and responds with what was requested (ex. a webpage). Dr Chuck illustrutes this example using a hyper link, saying that when it is clicked it makes a request to the server to complete that action and send back a webpage in HTML.
 
4. What is the IETF? What is an RFC?

The IETF is the Internet Engineering Task Force. They made the standards for all internet protocols. These standards are named the RFC or Request For Commits. The standards describe how the internet protocol functions.

5. In the video titled Worked Example: Sockets, Dr. Chuck tells us where to download a large collection of sameple programs he has available associated with the course. Where do we find these examples?

You can find these examples by going to https://www.py4e.com/materials.php and download the sample code zip file.

6. Try the telnet example that Dr. Chuck shows us in the Worked Example: Sockets video. Can you retrieve the document using telnet? (NOTE: examples in the previous videos no longer work. I suspect this is because HTTP/1.0 is no longer supported by the webserver)

Yes I can retrieve the document using telnet.

.

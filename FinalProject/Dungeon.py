import random
import os
import time
from sys import exit
from os import system
from Spider import spider
from Bear import bear
from Orc import orc
from Zombie import zombie
from Dragon import dragon

enemy = [spider, bear, orc, zombie]
os.system('clear')

print("Hello Adventurer! This is a short text based rpg with randomized enemies and encounters. Every enemy has a different amount of health and you have a random change to encounter each of them. You start every fight with 30 health.")
print("To move: you will be given choices of left, right, or a yes or no question.")
print("To fight: attacking will do somewhere between 1 and 7 damage, with a chance to also miss your attack. You can also heal with the heal command and heal somewhere between 10 and 20 health")

print("\n\n\n You walk up to the entrance of a cave, you need the treasure in the cave to feed your family, but you know it will be dangerous. Will you enter the cave?")


do_what = input("> ").lower()
if "yes" in do_what:
    os.system('clear')
    print("You walk into to cave. You hear something rustling. What is it?")
    time.sleep()
    random.choice(enemy)()
    time.sleep(6)
    os.system('clear')
    print("After defeating that monster you come across an intersection.\nDo you head left or right?")
    do_what = input("> ").lower()
    if "left" in do_what:
        print("Damn. A dead end. You start to head back but there is something blocking your path...")
        time.sleep(3)
        random.choice(enemy)()
        time.sleep(6)
        os.system('clear')
        print("After dealing with the monster you head back and go the other way, hopefully this one isnt a dead end either...")
        time.sleep(5)
        print("Another intersection? What is this? Before you can even think about what direction to head something approaches you....")
        time.sleep(3)
        random.choice(enemy)()
        time.sleep(6)
        os.system('clear')
        print("Whew. That was close. Actually I dont know, I wasn't looking. Shoot where were you? Oh thats right! An Intersection! uh left or right?")
        do_what = input("> ").lower()
        if "left" in do_what:
            print("You see a light at the end of the cave. You walk into the room with- wait no. Your not there yet. I must have did something wrong one second")
            time.sleep(3)
            print("I got it now. Before you can reach the room a MONSTER approaches you, OH NO!! WHAT IS IT!!")
            time.sleep(3)
            random.choice(enemy)()
            time.sleep(6)
            os.system('clear')
            print("Ok Ok OK I have it right now. You walk into a room with the a dragon sitting on the dragon, you- wait WHAT?")
            time.sleep(4)
            print("THE DRAGON IS ONLY 10 HEALTH??")
            print("Thats waaay to easy. Now who else can be the boss?")
            time.sleep(7)
            print("So apparently, no one is available. Guess i'll fight you my self")
            dragon()
            print("You killed me?? You idiot, now you'll have no one to narrat...")
            time.sleep(7)
    elif "right" in do_what:
        print("Another intersection? What is this? Before you can even think about what direction to head something approaches you....")
        time.sleep(3)
        random.choice(enemy)()
        time.sleep(6)
        os.system('clear')
        print("Whew. That was close. Actually I dont know, I wasn't looking. Shoot where were you? Oh thats right! An Intersection! uh left or right?")
        do_what = input("> ").lower()
        if "left" in do_what:
            print("You see a light at the end of the cave. You walk into the room with- wait no. Your not there yet. I must have did something wrong one second")
            time.sleep(4)
            print("I got it now. Before you can reach the room a MONSTER approaches you, OH NO!! WHAT IS IT!!")
            time.sleep(3)
            random.choice(enemy)()
            time.sleep(6)
            os.system('clear')
            print("Ok Ok OK I have it right now. You walk into a room with the a dragon sitting on the dragon, you- wait WHAT?")
            time.sleep(4)
            print("THE DRAGON IS ONLY 10 HEALTH??")
            print("Thats waaay to easy. Now who else can be the boss?")
            time.sleep(7)
            print("So apparently, no one is available. Guess i'll fight you my self")
            dragon()
            print("You killed me?? You idiot, now you'll have no one to narrat...")
            time.sleep(7)
        elif "right" in do_what:
            print("Nervous from your last fight you sprint down the hallway and run straight into a rock.")
            time.sleep(3)
            print("idiot")
            time.sleep(4)
            print("heres a monster i guess..")
            time.sleep(3)
            random.choice(enemy)()
            time.sleep(6)
            os.system('clear')
            print("You head back to the intersection and head left. Your on track now.")
            print("You see a light at the end of the cave. You walk into the room with- wait no. Your not there yet. I must have did something wrong one second")
            time.sleep(4)
            print("I got it now. Before you can reach the room a MONSTER approaches you, OH NO!! WHAT IS IT!!")
            time.sleep(3)
            random.choice(enemy)()
            time.sleep(6)
            os.system('clear')
            print("Ok Ok OK I have it right now. You walk into a room with the a dragon sitting on the dragon, you- wait WHAT?")
            time.sleep(4)
            print("THE DRAGON IS ONLY 10 HEALTH??")
            print("Thats waaay to easy. Now who else can be the boss?")
            time.sleep(7)
            print("So apparently, no one is available. Guess i'll fight you my self")
            dragon()
            time.sleep(7)
else:
    print("You do not enter the cave. You and your family starve to death.")

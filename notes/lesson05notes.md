1. Markdown is a lightweight markup language that can easily make documents that are easy to read and look nice. It will also translate and any md file to an html file to be displayed on a webpage in Git.

2. When Dr. Chuck describes functions as "store and reused patterns", he is reffering to how you can link lines of code to a function so you can use the function instead of the 5 lines of code. This is helpful as if there is a problem in those lines of code that are used in many places, you just need to edit the function defintion, not every single time it is used.

3. To create functions in Python we use the keyword def to define the function name, then add the commands or equations that will attached to the function. Example
	

	def functionname():
 
		print('this is a function')

		print('still a function')


4. Dr.Chuck calls the process that makes the function run two different words, calling and invoking. These words basically mean that they are "calling" back to the definition of the function and running those lines of code. 

5. Examples of built-in functions in python. print(), input(), type(), float(), int(), sum(), list(), and vars()


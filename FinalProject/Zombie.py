import random
import os
from sys import exit
from os import system

def zombie():
    play_again = True
    print("\nIts a Zombie!")
    while play_again:
        winner = None
        player_health = 30
        enemy_health = 10

        # determine whose turn it is
        turn = random.randint(1,2) # heads or tails
        if turn == 1:
            player_turn = True
            enemy_turn = False
            print("\nPlayer will go first.")
        else:
            player_turn = False
            enemy_turn = True
            print("\nZombie will go first.")


        print("\nPlayer health: ", player_health, "Zombie health: ", enemy_health)

        while (player_health != 0 or enemy_health != 0):

            heal_up = False
            miss = False

            if player_turn:
                print("\nPlease select a move:\nAttack\nHeal\n")

                player_move = input("> ").lower()

                move_miss = random.randint(1,5) # 20% of missing
                if move_miss == 1:
                    miss = True
                else:
                    miss = False

                if miss:
                    player_move = 0 # player misses and deals no damage
                    print("Move failed!")
                else:
                    if player_move in ("attack"):
                        player_move = random.randint(1, 7)
                        print("\nYou Atacked the zombie. It dealt ", player_move, " damage.")
                    elif player_move in ("heal"):
                        heal_up = True # heal activated
                        player_move = random.randint(10, 20)
                        print("\nYou used Heal. It healed for ", player_move, " health.")
                    else:
                        print("\nThat is not a valid move. Please try again.")
                        continue


            else:
                move_miss = random.randint(1,5)
                if move_miss == 1:
                    miss = True
                else:
                    miss = False

                if miss:
                    enemy_move = 0
                    print("The zombie missed!")
                else:
                    enemy_move = random.randint(1,7)
                    print("\nThe zombie hit you. It dealt ", enemy_move, " damage.")

            if heal_up:
                player_health += player_move
                if player_health > 30:
                    player_health = 30

            else:
                if player_turn:
                    enemy_health -= player_move
                    if enemy_health < 1:
                        enemy_health = 0
                        winner = "Player"
                        break
                else:
                    player_health -= enemy_move
                    if player_health < 1:
                        player_health = 0
                        winner = "zombie"
                        break

            print("\nPlayer health: ", player_health, "Zombie health: ", enemy_health)

            # switch turns
            player_turn = not player_turn
            enemy_turn = not enemy_turn

        if winner == "Player":
            print("\nPlayer health: ", player_health, "Zombie health: ", enemy_health)
            print("\nCongratulations! You have won.")
            play_again = False
        else:
            print("\nPlayer health: ", player_health, "Zombie health: ", enemy_health)
            print("\nSorry, but your opponent wiped the floor with you. Better luck next time.")
            play_again = False

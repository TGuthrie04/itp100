1. Dr. Chuck calls looping the 4th basic pattern of computer programming. What are the other three patterns?

The other three patterns are caled sequential, conditional, and store and reuse.

2. Describe the program Chr. Chuck uses to introduce the while loop. What does it do?

Dr. Chuck's program uses the while to have it countdown from 5. It does this by saying while n>0 subtract 1 and repeat the process until n=0. When n=0 it will print 'Blastoff!'

3. What is another word for looping introduced in the video?

Iterating

4. Dr. Chuck shows us a broken example of a loop. What is this kind of loop called? What is wrong with it?

This kind of loop is called an infinte loop. It will run forever and has no end, hence infinte. It will run untill your computer dies.

5. What Python statement (a new keyword for us, by the way) can be used to exit a loop?

Break can be used to exit a loop

6. What is the other loop control statement introduced in the first video? What does it do?

Continue is the other loop control statement introduced. It ends the current iteration and goes to the top of the loop and starts the next iteration.

7. Dr. Chuck ends the first video by saying that a while is what type of loop? Why are they called that? What is the type of loop he says will be discussed next?

While is an indefinite loop, it is called that because they keep going until a logical condition becomes false. Dr Chuck says we will be talking about definite loops next.

8. Which new Python loop is introduced in the second video? How does it work?

The "for" loop is introduced in the second video, it works by executing a program an exact number of times, rather than an indefinte loop which will repear forever until a logical condition becomes false.

9. The third video introduces what Dr. Chuck calls loop idioms. What does he mean by that term? Which examples of loop idioms does he introduce in this and the fourth video?

Loop idioms are patterns that have to do with how we construct loops. He introduces the example of finding the laregest number out of a group of numbers using loop idioms. He also demonstrated that you can make it find the smallest number by flipping the greater than sign to a less than sign and (technically optionally) changing the varible name from largest_so_far to smallest_so_far.


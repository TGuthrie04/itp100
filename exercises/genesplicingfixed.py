f = open("gene_splicing_test.dat")
line = f.readline()

for line in f:
    values = line.split()
    trials = 0
    x = float(values[0])
    y = float(values[1])
    c = float(values[2])
    t = float(values[3])

    check = 0
    ratio1 = 0
    ratio2 = 0
    target = x/y
    virus1 = [x,0]
    virus2 = [0,y]
    #defining the target and making a list for each virus with one value being its native virus or starting value, and the other value being the other virus mixed in. This s done so it can be eaiser to calculate the ratio at the end. 

    while True:
        #the check is so that the loop will repeat until it is done
        trials += 1
        #this will tell the scientits how many trials they need to do to get a mixture that is withing the tolerance

        virus2[0] += virus1[0]*(c/(virus1[0]+virus1[1]))
        virus2[1] += virus1[1]*(c/(virus1[0]+virus1[1]))
        #adding virus one to virus two, using the ratio of c (the amt we are moving and mixing) over the amt of virus in the virus 1 cell, this makes it proportional to the proportions given with c

        total = virus1[0]+virus1[1]

    #defines the constant of the total ml in virus one

        virus1[0] -= virus1[0]*(c/(total))   
        virus1[1] -= virus1[1]*(c/(total))

        #taking away the virus added to virus2 (proportianlly)

        virus1[0] += virus2[0]*(c/(virus2[0]+virus2[1]))
        virus1[1] += virus2[1]*(c/(virus2[0]+virus2[1]))

        #adding virus two to virus one

        total = virus2[0]+virus2[1]
        
        virus2[0] -= virus2[0]*(c/(total))
        virus2[1] -= virus2[1]*(c/(total))

        #taking away the virus added to virus1 (proportionally)

        ratio1 = virus1[0]/virus1[1]
        ratio2 = virus2[0]/virus2[1]

        #defining the ratios

        if abs(ratio1-target) <= t and abs(ratio2-target) <= t:
            break

        #if it doesnt find the ratio withing the tolerence then it will run the while loop over again

    print(trials)



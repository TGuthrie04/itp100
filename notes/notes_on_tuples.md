1. Dr. Chuck begins this lesson by stating that tuples are really a lot like lists. In what ways are tuples like lists? Do you agree with his statement that they are a lot alike?

Tuples and Lists store data and define themselves in almost the exact same way. They both have a set of strings that gets assigned a number that can be called on. Even though they do have difference I do agree with his statement that they are alot alike.

2. How do tuples essentially differ from lists? Why do you think there is a need for this additional data type if it is similar to a list?

Tuples differ from lists as they can not be modified. This is an important difference as when makin a program with many lists you can instead use tuples if you are not modifiying them and it will heavily decrease the memory needed to run it.

3. Dr. Chuck says that tuple assignment is really cool (Your instructor completely agrees with him, btw). Describe what tuple assignment is, showing a few examples of it in use. Do you think it is really cool? Why or why not?

With tuple assignment you can assign different constants to different values in a list like shown below

(a, b) = (apple, banana)

4. Summarize what you learned from the second video in this lesson. Provide example code to make support what you describe.

Some things we learned in the second video are...

- Sorting Lists of Tuples
	By  using the items() and sorted() functions, we can sort a list of tuples to get us a sorted version of a dictonary

- Sorting by values instead of key
	We learned to sort a list of suples by the value (using [key,value]). We learn to flip (key, value) to (value,key) then use the sorted function to sort it.

We also learned a shorter way to do it by using...

print(sorted( [ (v,k) for k,v in c.items() [ ) ) 

5.In the slide titled Even Shorter Version Dr. Chuck introduces list comprehensions. This is another really cool feature of Python. Did the example make sense to you? Do you think you understand what is going on?

I understand what is going on in the example, but I am having trouble understanding the concept as a whole.



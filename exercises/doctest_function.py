def ate_for_breakfast(food1, food2):
        """
        >>> ate_for_breakfast('eggs', 'hash browns')
        'I ate eggs and hash browns for breakfast.'
        >>> ate_for_breakfast('fruit', 'cereal')
        'I ate fruit and cereal for breakfast.'
        >>> ate_for_breakfast('pickles', 'ice cream')
        'I ate pickles and ice cream for breakfast.'
        """

        return 'I ate ' + food1 + ' and ' + food2 + ' for breakfast.'


        if __name__ == '__main__':
                import doctest
                doctest.testmod()
        


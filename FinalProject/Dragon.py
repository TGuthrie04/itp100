import random
import os
from sys import exit
from os import system

def dragon():
    play_again = True

    while play_again:
        winner = None
        player_health = 30
        enemy_health = 40

        # determine whose turn it is
        turn = random.randint(1,2) # heads or tails
        if turn == 1:
            player_turn = True
            enemy_turn = False
            print("\nPlayer will go first.")
        else:
            player_turn = False
            enemy_turn = True
            print("The Voice will go first.")


        print("\nPlayer health: ", player_health, "Voice health: ", enemy_health)

        while (player_health != 0 or enemy_health != 0):

            heal_up = False
            miss = False

            if player_turn:
                print("\nPlease select a move:\nAttack\nHeal\n")

                player_move = input("> ").lower()

                move_miss = random.randint(1,5) # 20% of missing
                if move_miss == 1:
                    miss = True
                else:
                    miss = False

                if miss:
                    player_move = 0 # player misses and deals no damage
                    print("Move failed!")
                else:
                    if player_move in ("attack"):
                        player_move = random.randint(4, 10)
                        print("\nYou Atacked the voice. It dealt ", player_move, " damage.")
                    elif player_move in ("heal"):
                        heal_up = True # heal activated
                        player_move = random.randint(10, 20)
                        print("\nYou used Heal. It healed for ", player_move, " health.")
                    else:
                        print("\nThat is not a valid move. Please try again.")
                        continue


            else:
                move_miss = random.randint(1,5)
                if move_miss == 1:
                    miss = True
                else:
                    miss = False

                if miss:
                    enemy_move = random.randint(5,10)
                    print("The voice missed! Wait no thats not possible, I don't miss! The voice hits anyway! ( just a little less because the voice feels bad...)")
                else:
                    enemy_move = random.randint(7,15)
                    print("\nThe voice hit you. It dealt ", enemy_move, " damage.")

            if heal_up:
                player_health += player_move
                if player_health > 30:
                    player_health = 30

            else:
                if player_turn:
                    enemy_health -= player_move
                    if enemy_health < 1:
                        enemy_health = 0
                        winner = "Player"
                        break
                else:
                    player_health -= enemy_move
                    if player_health < 1:
                        player_health = 0
                        winner = "Voice"
                        break

            print("\nPlayer health: ", player_health, "Voice health: ", enemy_health)

            # switch turns
            player_turn = not player_turn
            enemy_turn = not enemy_turn

        if winner == "Player":
            print("\nPlayer health: ", player_health, "Voice health: ", enemy_health)
            print("\nCongratulations! You have won.")
            print("You killed me?? You idiot, now you'll have no one to narrat...")

        else:
            print("\nPlayer health: ", player_health, "Voice health: ", enemy_health)
            print("\nTry again, python3 Dungeon.py baby")

        play_again = False

1. What are regular expressions? What are they used for? When did they originate?

Regular expressions are markdown language that is made up of markers of different things. These markers don't match what they mean usually, so the language as a whole very cryptic. Regular expressions are used to represent different commands in a smaller but more complicated form. This orginated as early as the 1960s and was a very old form of programming 

2. Use Markdown to reproduce the Regular Expression Quick Guide slide.

^\<^\>\s\s\s\s\s\s\s\s\A Matches the beginning of a line \Z $
^\<$\>\s\s\s\s\s\s\s\s\A Matches the end of a line \Z $
^\<.\>\s\s\s\s\s\s\s\s\A Matches any character \Z $
^\<\s\>\s\s\s\s\s\s\s\s\A Matches whitespace \Z $
^\<\S\>\s\s\s\s\s\s\s\s\A Matches any non whitespace character \Z $
^\<*\>\s\s\s\s\s\s\s\s\A Repeats a character zero or more times \Z $
^\<*?\>\s\s\s\s\s\s\s\s\A Repeats a character zero or more times (non-greedy)\Z $
^\<+\>\s\s\s\s\s\s\s\s\A Repeats a character one or more times \Z $
^\<+?\>\s\s\s\s\s\s\s\s\A Repeats a character one or more times (non-greedy) \Z $
^\<.\>\s\s\s\s\s\s\s\s\A Matches a single character in the listed set \Z $
^\<.\>\s\s\s\s\s\s\s\s\A Matches a single character not in the listed set \Z $
^\<.\>\s\s\s\s\s\s\s\s\A The set of characters can inlclude a range\Z $
^\<.\>\s\s\s\s\s\s\s\s\A Indicates where string extraction is to start \Z $
^\<.\>\s\s\s\s\s\s\s\s\A Indicates where string extraction is to end \Z $

still working on regex


4. Do a web search and find a few good resources for using regular expressions in vim.

https://developers.google.com/edu/python/regular-expressions
http://vimregex.com/
https://learnbyexample.gitbooks.io/vim-reference/content/Regular_Expressions.html


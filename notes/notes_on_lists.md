1. In the first video of this lesson, Dr. Chuck discusses two very important concepts: algorithms and data structures. How does he define these two concepts? Which one does he say we have been focusing on until now?

He defines thes concepts by telling us that Algoritms are a set of rules or steps used to solve a problem, which is what we have been focusing on. He defines data structures by saying they are a [articular way of organizing data in a computer.

2. Describe in your own words what a collection is.

Unlike a variable, a collection can hild multiple values in as a single varible. An example of this is lists like these

foods = [ `Hamburger`, `HotDog`, `Banana` ] 

3. Dr. Chuck makes a very important point in the slide labeled Lists and definite loops - best pals about Python and variable names that he has made before, but which bares repeating. What is that point?

Python can not understand plurals, i.e. friends vs friend. Any varible can replace friend and friends as python just goes in the order of the list.

4. How do we access individual items in a list?

By using the command 

print(listname[0])

This is assuming that listname is already defined
This will pull the first value from that list as it starts at 0 instead of 1

5. What does mutable mean? Provide examples of mutable and immutable data types in Python.

Mutable means that something can be changed; it is changeable. Strings are immutable and lists are mutable

6. Discribe several list operations presented in the lecture.

len() - tells you how many items/elements are in a list (not characters)

range() - gives a list of number that correspond to the element in your list. It would return something like [0, 1, 2, 3]

adding lists + - does not mathmatically add, rather looks to its left and right and puts the together ex. 

a = [1,2]
b = [3,4]
c = a + b 
print(c)
[1,2,3,4]

Slicing lists - works the same way as string, give it a starting point and an ending point (UP TO BUT NOT INCLUDING)

7. Discribe several list methods presented in the lecture. (you can find all by using the dir command)

count - looks for certain values in a list
extend - adds things to the end of the list
index - looks things up in the list
insert - allows things to be inserted into the middle to expand the list
pop - pulls things off the top
remove - removes an item in the middle
reverse - flips order 
sort - puts them into order based on values

8. Discribe several useful functions that take lists as arguments presented in the lecture.

len - finds length
max - finds largest value
min - finds smallest value
sum - adds them all up

9. The third video describes several methods that allow lists and strings to interoperate in very useful ways. Describe these.

split() - breaks a string into a lists by finding the spaces and seperating each term that has a space in between. Ex.

abc = `With three words`
stuff = abc.split()
print(stuff)
['With', 'three', 'words']

You can also tell python what character to split on by putting a character inside the parenthesis.

You can also do a double split where you can split a string into different strings then split that string again (using a different character)

10. What is a guardian pattern? Use at least one specific example in describing this important concept.

A guardian protects a command that could blow up if something goes wrong. It is a vital concept in program. Ex.

han open ('mbox-short.txt')

for line in han:
	line = line.rstrip() 
	print('Line:',line)
	wds = line.split()

# The Guardian Program 

if len(wds) < 1 :
	continue

# Back to code

if wds[0] != "From" :
	print('Ignore')
	continue
print(wds[2])


